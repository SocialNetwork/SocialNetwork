# Social Network Project - _serverless_ Backend

## Initial deployment steps

- npm run deploy _(only this command for subsequent deployments)_

- Modify the cors config of the s3 bucket to:
    ```xml
  <?xml version="1.0" encoding="UTF-8"?>
    <CORSConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
        <CORSRule>
            <AllowedOrigin>*</AllowedOrigin>
            <AllowedMethod>GET</AllowedMethod>
            <AllowedMethod>PUT</AllowedMethod>
            <MaxAgeSeconds>3000</MaxAgeSeconds>
            <AllowedHeader>*</AllowedHeader>
        </CORSRule>
    </CORSConfiguration>
    ```
    
- Add the following bucket policy to allow public access (serverless bug, as it's defined?)
    ```json
    {
    	"Version": "2012-10-17",
    	"Statement": [
    		{
    			"Sid": "AddPerm",
    			"Effect": "Allow",
    			"Principal": "*",
    			"Action": [
    				"s3:GetObject"
    			],
    			"Resource": [
    				"arn:aws:s3:::dev-jdekegel-socialnetwork/*"
    			]
    		}
    	]
    }
    ```

## Known issues

- There's a known issue when using custom authorizers in api gateway (which this project relies heavily upon),
the cors headers aren't sent when there's any error [more info on SO](http://stackoverflow.com/a/36922233/620141)

- lambda's aren't scoped properly yet, increasing the lambda filesize 