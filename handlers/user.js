'use strict';
const respond = require('../lib/responseWrapper')
const ManagementClient = require('auth0').ManagementClient;

const management = new ManagementClient({
  token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IlJUbEdRVFJHT0RVeE1Ea3lSVVZHTWpaQ01qazROakZETlRVME5FVTNOREF5UVVJNE56SkVRdyJ9.eyJpc3MiOiJodHRwczovL2pvbmFzZC5ldS5hdXRoMC5jb20vIiwic3ViIjoiZGJBWHVsa1M0NGp2dFVvRjhrNFdaelZvZnFvYXNHMm5AY2xpZW50cyIsImF1ZCI6Imh0dHBzOi8vam9uYXNkLmV1LmF1dGgwLmNvbS9hcGkvdjIvIiwiZXhwIjoxNTExNzI3NjI3LCJpYXQiOjE0ODg4MzE2MjcsInNjb3BlIjoicmVhZDpjbGllbnRfZ3JhbnRzIGNyZWF0ZTpjbGllbnRfZ3JhbnRzIGRlbGV0ZTpjbGllbnRfZ3JhbnRzIHVwZGF0ZTpjbGllbnRfZ3JhbnRzIHJlYWQ6dXNlcnMgdXBkYXRlOnVzZXJzIGRlbGV0ZTp1c2VycyBjcmVhdGU6dXNlcnMgcmVhZDp1c2Vyc19hcHBfbWV0YWRhdGEgdXBkYXRlOnVzZXJzX2FwcF9tZXRhZGF0YSBkZWxldGU6dXNlcnNfYXBwX21ldGFkYXRhIGNyZWF0ZTp1c2Vyc19hcHBfbWV0YWRhdGEgY3JlYXRlOnVzZXJfdGlja2V0cyByZWFkOmNsaWVudHMgdXBkYXRlOmNsaWVudHMgZGVsZXRlOmNsaWVudHMgY3JlYXRlOmNsaWVudHMgcmVhZDpjbGllbnRfa2V5cyB1cGRhdGU6Y2xpZW50X2tleXMgZGVsZXRlOmNsaWVudF9rZXlzIGNyZWF0ZTpjbGllbnRfa2V5cyByZWFkOmNvbm5lY3Rpb25zIHVwZGF0ZTpjb25uZWN0aW9ucyBkZWxldGU6Y29ubmVjdGlvbnMgY3JlYXRlOmNvbm5lY3Rpb25zIHJlYWQ6cmVzb3VyY2Vfc2VydmVycyB1cGRhdGU6cmVzb3VyY2Vfc2VydmVycyBkZWxldGU6cmVzb3VyY2Vfc2VydmVycyBjcmVhdGU6cmVzb3VyY2Vfc2VydmVycyByZWFkOmRldmljZV9jcmVkZW50aWFscyB1cGRhdGU6ZGV2aWNlX2NyZWRlbnRpYWxzIGRlbGV0ZTpkZXZpY2VfY3JlZGVudGlhbHMgY3JlYXRlOmRldmljZV9jcmVkZW50aWFscyByZWFkOnJ1bGVzIHVwZGF0ZTpydWxlcyBkZWxldGU6cnVsZXMgY3JlYXRlOnJ1bGVzIHJlYWQ6ZW1haWxfcHJvdmlkZXIgdXBkYXRlOmVtYWlsX3Byb3ZpZGVyIGRlbGV0ZTplbWFpbF9wcm92aWRlciBjcmVhdGU6ZW1haWxfcHJvdmlkZXIgYmxhY2tsaXN0OnRva2VucyByZWFkOnN0YXRzIHJlYWQ6dGVuYW50X3NldHRpbmdzIHVwZGF0ZTp0ZW5hbnRfc2V0dGluZ3MgcmVhZDpsb2dzIHJlYWQ6c2hpZWxkcyBjcmVhdGU6c2hpZWxkcyBkZWxldGU6c2hpZWxkcyByZWFkOmdyYW50cyBkZWxldGU6Z3JhbnRzIHJlYWQ6Z3VhcmRpYW5fZmFjdG9ycyB1cGRhdGU6Z3VhcmRpYW5fZmFjdG9ycyByZWFkOmd1YXJkaWFuX2Vucm9sbG1lbnRzIGRlbGV0ZTpndWFyZGlhbl9lbnJvbGxtZW50cyBjcmVhdGU6Z3VhcmRpYW5fZW5yb2xsbWVudF90aWNrZXRzIHJlYWQ6dXNlcl9pZHBfdG9rZW5zIn0.pIOp5j_bsTGcVEua2iWIEprN5fJxzgKjs2n3Adlkb4Ubx5dK0j2UtEC-NL0XvZFC_oYnlif-cmOyREoCvF2IcmEjf-UbX8S1bkhVb71S2K9NFhGoPhnR09HezaOShVREB7apYI1g1mGCofznqOcQmSTSaSI_L9BMfM0thNiBvfT97TWiSPa83pOWaJhV98alBgXNLAnsESxKiK7pMeh4f0b0ZYrtN9mXCpXiBNyybbJHUZSzU8opLGxHE1Er1Xa0N1jjWfYgn7YHyWRQ1jorw6TuKkDMWDUGKKRqX6WrCjlvL0BHmtQDF4HHSnPvgxeuXTI7vGnyJfW9ZTv-Ygjn0w',
  domain: 'jonasd.eu.auth0.com'
});

const getUser = ({pathParameters:{id}}, context, callback) => {

  management.getUser({id})
    .then(({name, nickname, user_id, picture}) => {
      respond(null, {name, nickname, user_id, picture}, callback);
    })
    .catch((err) => {
      console.log(err);
    })

};

const getUsers = (event, context, callback) => {

  management.getUsers()
    .then((users) => {
      respond(null, users.map(({name, nickname, user_id, picture, user_metadata, identities:[{provider}]}) => {
        return {name, nickname, user_id, picture, user_metadata, provider}
      }), callback);
    })
    .catch((err) => {
      console.log(err);
    })

};

const getUsersKey = (event, context, callback) => {

  management.getUsers()
    .then((users) => {
      let ret = Array.from(new Map(users.map(({name, nickname, user_id, picture, user_metadata, identities:[{provider}]}) => {
        return [user_id, {user_id, name, nickname, picture, user_metadata, provider}]
      })))
      respond(null, ret, callback);
    })
    .catch((err) => {
      console.log(err);
    })

};

export {
  getUser,
  getUsers,
  getUsersKey
}