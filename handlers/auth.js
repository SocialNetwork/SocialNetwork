/**
 * Created by jonas on 05/03/2017.
 */
const jwt = require('jsonwebtoken');

const AUTH0_CLIENT_ID = 'JBk5E6LbwkGCLpLxQBw7oHGKZ5AaILwy';
const AUTH0_CLIENT_SECRET = 'eFzJy5UGldje7CyaQzqK_iSt7RG8MpIA4aGnqCgVs0Co44dNw5cSkJVOHOGYzFkH';

// Policy helper function
const generatePolicy = (principalId, effect, resource) => {
  const authResponse = {};
  authResponse.principalId = principalId;
  if (effect && resource) {
    const policyDocument = {};
    policyDocument.Version = '2012-10-17';
    policyDocument.Statement = [];
    const statementOne = {};
    statementOne.Action = 'execute-api:Invoke';
    statementOne.Effect = effect;
    statementOne.Resource = resource;
    policyDocument.Statement[0] = statementOne;
    authResponse.policyDocument = policyDocument;
  }
  return authResponse;
};

// Reusable Authorizer function, set on `authorizer` field in serverless.yml
module.exports.handler = (event, context, cb) => {
  if (event.authorizationToken) {
    // remove "bearer " from token
    const token = event.authorizationToken.substring(7);
    const options = {
      audience: AUTH0_CLIENT_ID,
    };
    jwt.verify(token, AUTH0_CLIENT_SECRET, options, (err, decoded) => {
      if (err) {
        cb(null, generatePolicy("Unauthorized", 'Deny', event.methodArn));
        // cb('Unauthorized');
      } else {
        cb(null, generatePolicy(decoded.sub, 'Allow', event.methodArn));
      }
    });
  } else {/**/
    cb('Unauthorized');
  }
};