'use strict';
const respond = require('../lib/responseWrapper')
const db = require('../lib/commentDB')
const photoDb = require('../lib/photoDB')

const addComment = ({pathParameters:{uuid}, requestContext:{authorizer:{principalId:owner}}, body}, context, callback) => {
  uuid = decodeURIComponent(uuid)
  body = JSON.parse(body)

  // respond(null, {comment}, callback)
  db.addComment(uuid, owner, body.comment)
    .then((res) => respond(null, {res}, callback))
    .catch(() => respond(new Error("[400] Invalid uuid/owner combination"), null, callback, 400))

};

const removeComment = ({pathParameters:{uuid}, requestContext:{authorizer:{principalId:owner}}}, context, callback) => {
  uuid = decodeURIComponent(uuid)

  db.getComment(uuid)
    .then((res) => {
      if (res && res.commentOn)
        return photoDb.getMeta(res.commentOn)
    })
    .then((res) => {
      if (res && res.owner_id == owner)
        return db.removeComment(uuid)
      else throw new Error("[400] Invalid uuid/Only the object owner can remove comments")
    })
    .then(() => respond(null, null, callback))
    // according to the docs the callback should fetch the statuscode from the [] in the error, but it doesn't
    .catch((err) => respond(err, null, callback, 400))

};

const getComments = ({pathParameters:{uuid}}, context, callback) => {
  uuid = decodeURIComponent(uuid)

  db.getCommentsByUuid(uuid)
    .then((comments) => {
      respond(null, {
        message: 'comments for ' + uuid,
        uuid,
        comments
      }, callback);
    })
    .catch((err) => {
      console.log(err);
    })

};

export {
  addComment,
  removeComment,
  getComments
}