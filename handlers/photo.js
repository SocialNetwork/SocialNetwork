'use strict';
const respond = require('../lib/responseWrapper')
const s3 = require('../lib/s3')
// const uuidV4 = require('uuid/v4')
const db = require('../lib/photoDB')


const getPhotoUploadURL = (event, context, callback) => {
  const owner = event.requestContext.authorizer.principalId
  return s3.getUrl(event.pathParameters.url)
    .then(({url, uuid}) => {
      console.log(" url", uuid, url)
      respond(null, {url, uuid}, callback)
      return uuid
    })
    .catch((err) => {
      console.log("getPhotoUploadPublicERR", err)
      respond(err, null, callback)
    })
    .then((uuid) => {
      console.log("uuid", uuid, owner)
      return db.createOrUpdateMeta(uuid, owner)
        .then((t) => console.log("test", t));
    })
    .catch((err) => {
      console.log("getPhotoUploadMetaERR", err)
    })
}

const getPhotosByOwner = (event, context, callback) => {
  const owner_id = decodeURIComponent(event.pathParameters.owner_id)

  db.getPhotosByOwner(owner_id)
    .then((data) => {
      console.log("querydata", data);
      respond(null, {
        message: 'photos name/description for ' + owner_id,
        data: data.Items.map((el) => {
          el.url = `${process.env.s3bucketurl}photos/${el.uuid}`
          return el
        }),
        event,
        context
      }, callback);
    })
    .catch((err) => {
      console.log(err);
    })


};


const getMeta = ({pathParameters:{uuid}}, context, callback) => {
  uuid = decodeURIComponent(uuid)

  db.getMeta(uuid)
    .then((data) => {
      console.log(data);
      respond(null, data, callback);
    })
    .catch((err) => console.log(err))

};

const postMeta = (event, context, callback) => {
  const {name, description} = JSON.parse(event.body) || {}
  const uuid = decodeURIComponent(event.pathParameters.uuid)
  const owner = event.requestContext.authorizer.principalId

  db.createOrUpdateMeta(uuid, owner, name, description)
    .then(() => respond(null, {uuid, owner, name, description}, callback))
    .catch(() => respond(new Error("[400] Invalid uuid/owner combination"), null, callback, 400))


  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // callback(null, { message: 'Go Serverless v1.0! Your function executed successfully!', event });
};

export {
  getPhotoUploadURL,
  getPhotosByOwner,
  getMeta,
  postMeta
}