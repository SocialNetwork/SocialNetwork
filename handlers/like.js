'use strict';
const respond = require('../lib/responseWrapper')
const db = require('../lib/likeDB')

const addLike = ({pathParameters:{uuid}, requestContext:{authorizer:{principalId:owner}}}, context, callback) => {
  uuid = decodeURIComponent(uuid)

  db.addLike(uuid, owner)
    .then(() => respond(null, null, callback))
    .catch(() => respond(new Error("[400] Invalid uuid/owner combination"), null, callback, 400))

};

const removeLike = ({pathParameters:{uuid}, requestContext:{authorizer:{principalId:owner}}}, context, callback) => {
  uuid = decodeURIComponent(uuid)

  db.removeLike(uuid, owner)
    .then(() => respond(null, null, callback))
    .catch(() => respond(new Error("[400] Invalid uuid/owner combination"), null, callback, 400))

};

const toggleLike = ({pathParameters:{uuid}, requestContext:{authorizer:{principalId:owner}}}, context, callback) => {
  uuid = decodeURIComponent(uuid)

  db.toggleLike(uuid, owner)
    .then((like) => respond(null, {uuid, like}, callback))
    .catch(() => respond(new Error("[400] Invalid uuid/owner combination"), null, callback, 400))

};

const hasLiked = ({pathParameters:{uuid}, requestContext:{authorizer:{principalId:owner}}}, context, callback) => {
  uuid = decodeURIComponent(uuid)

  db.hasLiked(uuid, owner)
    .then((hasLiked) => {
      respond(null, {
        message: `${owner} has${(hasLiked) ? '' : ' NOT'} liked ${uuid}`,
        uuid,
        hasLiked
      }, callback);
    })
    .catch((err) => {
      console.log(err);
    })

};

const getLikesCount = ({pathParameters:{uuid}}, context, callback) => {
  uuid = decodeURIComponent(uuid)

  db.getLikesCountForItem(uuid)
    .then((count) => {
      respond(null, {
        message: 'likes count for ' + uuid,
        uuid,
        count
      }, callback);
    })
    .catch((err) => {
      console.log(err);
    })

};

export {
  addLike,
  removeLike,
  toggleLike,
  hasLiked,
  getLikesCount
}