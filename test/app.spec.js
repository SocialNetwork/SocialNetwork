/**
 * Created by jonas on 02/03/2017.
 */

describe("Core app", () => {

  context("should allow a user", () => {
    it("to sign up")

    it("to upload a photo")

    it("to upload photos")

    it("to add/edit a photo name/description")

    it("to comment on a(nother) user's photo")

    it("to like a(nother) user's photo")

    it("to view a userlist")

    it("to remove comments on his own photos")
  })
})