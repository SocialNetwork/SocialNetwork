/**
 * Created by jonas on 04/03/2017.
 */
'use strict';
const dynamodb = require('serverless-dynamodb-client');

const TableName = process.env.likeTableName

const docClient = dynamodb.doc;

const addLike = (likeOn, likeFrom) => {

  const params = {
    TableName,
    Item: {
      likeOn,
      likeFrom
    }
  };

  return docClient.put(params).promise()
    .then(() => 1)

};

const removeLike = (likeOn, likeFrom) => {

  const params = {
    TableName,
    Key: {
      likeOn,
      likeFrom
    }
  };


  return docClient.delete(params).promise()
    .then(() => 0)

};

const toggleLike = (likeOn, likeFrom) => {
  return hasLiked(likeOn, likeFrom)
    .then((hasLiked) => {
      console.log(hasLiked)
      if (hasLiked)
        return removeLike(likeOn, likeFrom)
      else
        return addLike(likeOn, likeFrom)
    })
}

const hasLiked = (likeOn, likeFrom) => {

  const params = {
    TableName,
    KeyConditionExpression: 'likeOn = :hkey and likeFrom = :rkey',
    ExpressionAttributeValues: {
      ':hkey': likeOn,
      ':rkey': likeFrom
    }
  };

  return docClient.query(params).promise()
    .then((data) => {
      return (data.Count)
    })

};

const getLikesCountForItem = (likeOn) => {

  const params = {
    TableName,
    KeyConditionExpression: 'likeOn = :hkey',
    ExpressionAttributeValues: {
      ':hkey': likeOn
    }
  };

  return docClient.query(params).promise()
    .then((data) => {
      return data.Count
    })

};

export {
  addLike,
  removeLike,
  toggleLike,
  hasLiked,
  getLikesCountForItem,
}