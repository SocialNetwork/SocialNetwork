/**
 * Created by jonas on 04/03/2017.
 */
const S3 = require('aws-sdk').S3
const uuidV4 = require("uuid/v4")
const s3 = new S3({signatureVersion: 'v4'})

const Bucket = process.env.BUCKETNAME
const PHOTOS_PATH = 'photos' // Bucket files path
// const PHOTO_TYPES = /\.(jpg|jpeg|png|gif)$/g // Allowed photo upload extensions

const GET_URL_PARAMS = {
  Bucket,
  // Key: PHOTOS_PATH + "/" + "uuid",
  Expires: 120
}

function getUrl(filename) {
  return new Promise(function (fulfill, reject) {
    const fileext = filename.substr(filename.lastIndexOf('.'))
    const uuidfilename = uuidV4() + fileext
    // console.log(fileext, PHOTO_TYPES.test(fileext))
    // if (PHOTO_TYPES.test(fileext)) { //bugged in AWS, console_log returned true but ended up in else
    if (fileext == ".jpg" || fileext == ".jpeg" || fileext == ".png" || fileext == ".gif") {
      let params = Object.assign({Key: `${PHOTOS_PATH}/${uuidfilename}`}, GET_URL_PARAMS)
      s3.getSignedUrl('putObject', params, function (err, url) {
        if (err) reject(err)
        else {
          fulfill({url, uuid: uuidfilename})
        }
      })
    } else
      reject("Please enter a valid photo filename")
  })
}

function keyExists(Key) {
  return s3.headObject({Bucket, Key}).promise()
}

module.exports = {getUrl, keyExists}