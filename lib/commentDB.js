/**
 * Created by jonas on 04/03/2017.
 */
'use strict';
const uuidV4 = require("uuid/v4")
const dynamodb = require('serverless-dynamodb-client');

const TableName = process.env.commentTableName
const IndexName = process.env.commentTableIndex

const docClient = dynamodb.doc;

const addComment = (commentOn, commentBy, comment) => {

  const uuid = uuidV4()
  const params = {
    TableName,
    Item: {
      uuid,
      commentOn,
      created: Date.now(),
      comment,
      commentBy,

    }
  };

  return docClient.put(params).promise()
    .then((res) => getComment(uuid))

};

const removeComment = (uuid) => {

  const params = {
    TableName,
    Key: {
      uuid
    }
  };


  return docClient.delete(params).promise()

};


const getComment = (uuid) => {

  const params = {
    TableName,
    KeyConditionExpression: '#uuid = :hkey',
    ExpressionAttributeNames: {
      '#uuid': 'uuid',
    },
    ExpressionAttributeValues: {
      ':hkey': uuid,
    }
  };

  return docClient.query(params).promise()
    .then((data) => data.Items[0])

};

const getCommentsByUuid = (commentOn, ScanIndexForward = true) => {

  const params = {
    TableName,
    IndexName,
    KeyConditionExpression: 'commentOn = :hkey',
    ScanIndexForward,
    ExpressionAttributeValues: {
      ':hkey': commentOn,
    }
  };

  return docClient.query(params).promise()

};

export {
  addComment,
  removeComment,
  getComment,
  getCommentsByUuid,
}