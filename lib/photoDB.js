/**
 * Created by jonas on 04/03/2017.
 */
'use strict';
// const AWS = require('aws-sdk')
// AWS.config.update({region: "eu-central-1"})
const dynamodb = require('serverless-dynamodb-client');
// const uuidV4 = require('uuid/v4')

const TableName = process.env.photoTableName

// const docClient = new AWS.DynamoDB.DocumentClient();
const docClient = dynamodb.doc;


const getPhotosByOwner = (owner_id) => {

  const params = {
    TableName,
    IndexName: 'ownerIndex',
    KeyConditionExpression: 'owner_id = :hkey',// and RangeKey > :rkey',
    ExpressionAttributeValues: {
      ':hkey': owner_id,
      // ':rkey': 2015
    }
  };

  return docClient.query(params).promise()

};


const getMeta = (uuid) => {

  const params = {
    TableName,
    KeyConditionExpression: '#uuid = :hkey',
    ExpressionAttributeNames: {
      '#uuid': 'uuid',
    },
    ExpressionAttributeValues: {
      ':hkey': uuid,
    }
  };

  return docClient.query(params).promise()
    .then((data) => {
      return data.Items[0]
      // return (data.Count == 1) ? data.Items[0] : {}
    })

};

const createOrUpdateMeta = (uuid, owner, name, description) => {
  return getMeta(uuid)
    .then((check) => {

      if (!check || check.owner_id == owner)
        return putMeta(uuid, owner, name, description)
          .catch((err) => {
            console.log("error putting meta", err)
          })
      else
        throw new Error("uuid/owner mismatch, uuid occupied by other user")
    })
}

const putMeta = (uuid, owner_id, name, description) => {

  const params = {
    TableName,
    Item: {
      uuid, //uuidV4()
      owner_id,
      name,
      description
      // HashKey: 'haskey',
      // NumAttribute: 1,
      // BoolAttribute: true,
      // ListAttribute: [1, 'two', false],
      // MapAttribute: { foo: 'bar'},
      // NullAttribute: null
    }
  };

  return docClient.put(params).promise()

};

export {
  getPhotosByOwner,
  getMeta,
  createOrUpdateMeta,
  putMeta,
}