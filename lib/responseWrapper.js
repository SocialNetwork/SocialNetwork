/**
 * Created by jonas on 04/03/2017.
 */

// const CORS_HEADERS = {
//   "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
//   "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS
// }

/**
 * Responsewrapper Api gateway - Lambda
 * @param err
 * @param body Object
 * @param callback
 * @param headers
 * @param cors Boolean, automatically includes cors headers, defaults to true
 * @param statusCode
 */
module.exports = function (err = null, body, callback, statusCode, headers = {}, cors = true) {
  if (!Number.isInteger(statusCode))
    statusCode = (err == null) ? 200 : 500
  if (cors) {
    if (!headers)
      headers = {}
    // headers = Object.assign(headers,CORS_HEADERS)
    headers["Access-Control-Allow-Origin"] = "*"
    headers["Access-Control-Allow-Credentials"] = true
  }
  callback(null, {
    statusCode,
    headers,
    body: JSON.stringify((err) ? {error: (statusCode == 500) ? "Internal Server Error" : err.message} : body)
  })
};